/*
 * Copyright (c) 2016-2018 The Aiicy Team
 * Licensed under The MIT License (MIT)
 * See: LICENSE
 */

fn main() {
    println!("Hello, world!");
}
