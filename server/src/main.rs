/*
 * Copyright (c) 2016-2018 The Aiicy Team
 * Licensed under The MIT License (MIT)
 * See: LICENSE
 */

extern crate clap;
use clap::{App, Arg, ArgMatches};

// argument name constants for the CLI options
const DNS_ARG: &str = "dns";
const DNSHOST_ARG: &str = "dnshost";
const DNSPORT_ARG: &str = "dnsport";
const PASSTHROUGH_ARG: &str = "passthrough";
const SECURITY_ARG: &str = "security";
const SECRET_ARG: &str = "secret";
const AUTO_COMMAND_ARG: &str = "auto-command";
const AUTO_ATTACH_ARG: &str = "auto-attach";
const PACKET_TRACE_ARG: &str = "packet-trace";
const PROCESS_ARG: &str = "process";
const HISTORY_SIZE_ARG: &str = "history-size";
const LISTENER_ARG: &str = "listener";
const FIREHOSE_ARG: &str = "firehose";
const CACHE_ARG: &str = "cache";

/// Args struct for all options
struct Args {
    pub flag_dns: String,
}

impl<'a> From<ArgMatches<'a>> for Args {
    fn from(matches: ArgMatches<'a>) -> Args {
        Args {
            flag_dns: matches
                .value_of(DNS_ARG)
                .map(|s| s.to_string())
                .expect("Should set a dns address"),
        }
    }
}
pub fn main() {
    let args = App::new("dnscat2")
        .version("0.0.1")
        .author("CountStarlight <countstarlight@gmail.com>")
        .about("Copyright (c) 2016-2018 The Aiicy Team")
        .usage(
            "\tYou'll almost certainly want to run this in one of a few ways...\n\
             \tDefault host (0.0.0.0) and port (53), with no specific domain:\n\
             \t# ./server\n\
             \tDefault host/port, with a particular domain to listen on:\n\
             \t# ./server domain.com\n\
             \tIf you need to change the address or port it's listening on, that\n\
             \tcan be done by passing the --dns argument:\n\
             \t# ./server --dns 'host=127.0.0.1,port=53531,domain=a.com,domain=b.com'\n\
             \tFor other options, see below!",
        )
        .arg(
            Arg::with_name(DNS_ARG)
                .short("d")
                .long(DNS_ARG)
                .help(
                    "Start a DNS server. Can optionally pass a number of comma-separated\n\
                     name=value pairs (host, port, domain). Eg, '--dns\n\
                     host=0.0.0.0,port=53531,domain=skullseclabs.org' - 'domain' can be\n\
                     passed multiple times",
                )
                .value_name("s"),
        )
        .arg(
            Arg::with_name(DNSHOST_ARG)
                .short("n")
                .long(DNSHOST_ARG)
                .help("The DNS ip address to listen on [deprecated]")
                .value_name("s")
                .default_value("0.0.0.0"),
        )
        .arg(
            Arg::with_name(DNSPORT_ARG)
                .short("s")
                .long(DNSPORT_ARG)
                .help("The DNS port to listen on [deprecated]")
                .value_name("i")
                .default_value("53")
        )
        .arg(
            Arg::with_name(PASSTHROUGH_ARG)
                .short("p")
                .long(PASSTHROUGH_ARG)
                .help("Unhandled requests are sent upstream DNS server, host:port")
                .value_name("s")
                .default_value("")
        )
        .arg(
            Arg::with_name(SECURITY_ARG)
                .short("e")
                .long(SECURITY_ARG)
                .help("Set the security level; 'open' lets the client choose; 'encrypted'\n\
                requires encryption (default if --secret isn't set); 'authenticated'\n\
                requires encryption and authentication (default if --secret is set)")
                .value_name("s")
        )
        .arg(
            Arg::with_name(SECRET_ARG)
                .short("c")
                .long(SECRET_ARG)
                .help("A pre-shared secret, passed to both the client and server to prevent\n\
                man-in-the-middle attacks")
                .value_name("s")
        )
        .arg(
            Arg::with_name(AUTO_COMMAND_ARG)
                .short("a")
                .long(AUTO_COMMAND_ARG)
                .help("Send this to each client that connects")
                .value_name("s")
                .default_value("")
        )
        .arg(
            Arg::with_name(AUTO_ATTACH_ARG)
                .short("u")
                .long(AUTO_ATTACH_ARG)
                .help("Automatically attach to new sessions")
        )
        .arg(
            Arg::with_name(PACKET_TRACE_ARG)
                .short("k")
                .long(PACKET_TRACE_ARG)
                .help("Display incoming/outgoing dnscat packets")
        )
        .arg(
            Arg::with_name(PROCESS_ARG)
                .short("r")
                .long(PROCESS_ARG)
                .help("If set, the given process is run for every incoming console/exec\n\
                session and given stdin/stdout. This has security implications.")
                .value_name("s")
        )
        .arg(
            Arg::with_name(HISTORY_SIZE_ARG)
                .short("i")
                .long(HISTORY_SIZE_ARG)
                .help("The number of lines of history that windows will maintain")
                .value_name("i")
                .default_value("1000")
        )
        .arg(
            Arg::with_name(LISTENER_ARG)
                .short("l")
                .long(LISTENER_ARG)
                .help("DEBUG: Start a listener driver on the given port")
                .value_name("i")
        )
        .arg(
            Arg::with_name(FIREHOSE_ARG)
                .short("f")
                .long(FIREHOSE_ARG)
                .help("If set, all output goes to stdout instead of being put in windows.")
        )
        .arg(
            Arg::with_name(CACHE_ARG)
                .long(CACHE_ARG)
                .help("If set, caching is enabled on the server.")
        )
        .get_matches();
    let args: Args = args.into();
}
