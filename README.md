# dnscat2-rs
A Rust port of [dnscat2](https://github.com/iagox86/dnscat2)

## To-do list

- [ ] Server
- [ ] Client

## Server

```shell
$ ./server -h
dnscat2 0.0.1
CountStarlight <countstarlight@gmail.com>
Copyright (c) 2016-2018 The Aiicy Team

USAGE:
        You'll almost certainly want to run this in one of a few ways...
        Default host (0.0.0.0) and port (53), with no specific domain:
        # ./server
        Default host/port, with a particular domain to listen on:
        # ./server domain.com
        If you need to change the address or port it's listening on, that
        can be done by passing the --dns argument:
        # ./server --dns 'host=127.0.0.1,port=53531,domain=a.com,domain=b.com'
        For other options, see below!

FLAGS:
    -u, --auto-attach     Automatically attach to new sessions
        --cache           If set, caching is enabled on the server.
    -f, --firehose        If set, all output goes to stdout instead of being put in windows.
    -h, --help            Prints help information
    -k, --packet-trace    Display incoming/outgoing dnscat packets
    -V, --version         Prints version information

OPTIONS:
    -a, --auto-command <s>    Send this to each client that connects [default: ]
    -d, --dns <s>             Start a DNS server. Can optionally pass a number of comma-separated
                              name=value pairs (host, port, domain). Eg, '--dns
                              host=0.0.0.0,port=53531,domain=skullseclabs.org' - 'domain' can be
                              passed multiple times
    -n, --dnshost <s>         The DNS ip address to listen on [deprecated] [default: 0.0.0.0]
    -s, --dnsport <i>         The DNS port to listen on [deprecated] [default: 53]
    -i, --history-size <i>    The number of lines of history that windows will maintain [default: 1000]
    -l, --listener <i>        DEBUG: Start a listener driver on the given port
    -p, --passthrough <s>     Unhandled requests are sent upstream DNS server, host:port [default: ]
    -r, --process <s>         If set, the given process is run for every incoming console/exec
                              session and given stdin/stdout. This has security implications.
    -c, --secret <s>          A pre-shared secret, passed to both the client and server to prevent
                              man-in-the-middle attacks
    -e, --security <s>        Set the security level; 'open' lets the client choose; 'encrypted'
                              requires encryption (default if --secret isn't set); 'authenticated'
                              requires encryption and authentication (default if --secret is set)
```